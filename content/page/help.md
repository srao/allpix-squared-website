---
template: overrides/main.html
title: "Get Involved"
---

Follow these links to know more about contributing to Allpix Squared:

- [Download Releases](https://project-allpix-squared.web.cern.ch/project-allpix-squared/releases/)
- [Forum](https://cern.ch/allpix-squared-forum/)
- [Issue Tracker](https://gitlab.cern.ch/allpix-squared/allpix-squared/issues)
- [Repository](https://gitlab.cern.ch/allpix-squared/allpix-squared)
- [Mailing Lists](https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=allpix-squared)