# Allpix Squared Website

This repository contains the static website pages of the Allpix Squared project. The deployed website can be found [here](https://cern.ch/allpix-squared)

All commits to the master branch of this repository are automatically deployed to the EOS space of the project and thus directly reflect on the published website.

## Dependencies

The website is generated from markdown files using the [Mkdocs](https://www.mkdocs.org/) compiler written in Python. In order to install Mkdocs on your machine, follow the [installation instructions](https://www.mkdocs.org/#installation). For detailed instructions on using the Material for Mkdocs theme, check out the [official documentation](https://squidfunk.github.io/mkdocs-material/getting-started/).

## Commands
The following commands might help adding content and testing the website generation locally before committing and deployment:

* __Start a local test server__
    ```
    mkdocs serve -f <config>.yml
    ```
    The server will listen on the address defined in the `config.yml` file, e.g. http://localhost:8000/. 

* __Add a new page__

    To create a new page, open a new file with following YAML header:
	```
	---
	template: overrides/main.html
	title: ""
    ---
	```
	Save this file as a markdown file `(.md)` in the `usermanual` folder.
	
* __Add a new post to the blog roll__

    To create a new blog post, open a new file with following YAML header:
	```
	---
	title: ""
	date: YYYY-MM-DD
    ---
	```
	Save this file as a markdown file `(.md)`in the `post` folder. The recommended file naming convention is `yyyy-mm-dd-post-title.md`. 

* __Add page to the main menu__

    The main menu is defined in the `mkdocs<>.yml` file. Simply add a new entry to the Page tree in the `nav` block.


## Deploy Changes

Simply push to the `master` branch of this repository to get things published.